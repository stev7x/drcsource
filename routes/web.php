<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    // Authentication Routes...
//    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/replikasi', 'ReplikasiController@index')->name('replikasi');
    Route::get('/replikasi/ajaxdata', 'ReplikasiController@ajaxData')->name('ajax_data_replikasi');

    Route::get('/failover', 'FailOverController@index')->name('failover');
    Route::get('/failover/ajaxdata', 'FailOverController@ajaxData')->name('ajax_data_failover');
    Route::get('/failover/ajaxchangemode', 'FailOverController@ajaxChangeMode')->name('ajax_changemode_failover');
    Route::get('/failover/at4test', 'FailOverController@testAT4')->name('test_at4');
    Route::get('/failover/currentstatreplikasi', 'FailOverController@currentStatReplikasi')->name('current_stat_replikasi');
    Route::get('/failover/currentstatdrc', 'FailOverController@currentStatDRC')->name('current_stat_drc');

    Route::get('/datalalin', 'DataLalinController@index')->name('datalalin');
    Route::get('/datalalin/upload', 'DataLalinController@upload')->name('datalalin_upload');
    Route::post('/datalalin/upload/process', 'DataLalinController@uploadProcess')->name('datalalin_upload_process');
});

Route::get('/api/heartbeat', 'FailOverController@heartbeat')->name('heartbeat');
Route::get('/test_replikasi', 'ReplikasiController@test_replikasi')->name('test_replikasi');

Route::get('/', 'ReplikasiController@direct_replikasi')->name('direct_replikasi');
Route::get('/direct_ajaxdata', 'ReplikasiController@direct_ajaxData')->name('direct_ajax_replikasi');



