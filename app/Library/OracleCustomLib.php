<?php

namespace App\Library;

use Illuminate\Support\Facades\DB;

class OracleCustomLib
{
    public function query($query)
    {
        try
        {
            DB::connection('oracle')->getPdo();
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }

        try
        {
            if ($response = DB::connection('oracle')->select($query))
            {
                return $response;
            }
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function getCabangName($id_cabang)
    {
        return $this->query("SELECT SHORT_NAME_CABANG FROM P_CABANG WHERE KODE_CABANG = $id_cabang")[0]->short_name_cabang;
    }

    public function getCabang()
    {
        return $this->query("SELECT a.kode_cabang, a.short_name_cabang, a.ip_cabang, a.start_redis_cabang, a.start_redis_drc, b.kode_cabang FROM p_ip_cabang a, DRC_T_REPLIKASI b WHERE a.kode_cabang = b.kode_cabang");
    }

    public function getLogDRC($id_cabang)
    {
        return $this->query("SELECT f_get_log_drc(".$id_cabang.") FROM dual");
    }

    public function getReplikasi($id_cabang)
    {
        return $this->query("SELECT * FROM DRC_T_REPLIKASI WHERE KODE_CABANG = ".$id_cabang)[0];
    }

    public function getReplikasiByKey($id_cabang, $key)
    {
        return $this->query("SELECT ".$key." FROM DRC_T_REPLIKASI WHERE KODE_CABANG = " . $id_cabang)[0];
    }

    public function updateReplikasi($id_cabang, $key, $value)
    {
        $this->query("UPDATE DRC_T_REPLIKASI SET ".$key." = ".$value." WHERE kode_cabang = ".$id_cabang);

        return 1;
    }

    public function getRedisConnection($id_cabang)
    {
        return $this->query("SELECT ip_drc, port_drc, b.paswword FROM p_ip_cabang a, p_port_cabang b WHERE a.kode_cabang = ".$id_cabang." AND b.KODE_CABANG = ".$id_cabang." AND b.KODE_BANK = 'MAIN'")[0];
    }

    public function getAllRedisConnection()
    {
        return $this->query("SELECT a.kode_cabang , a.short_name_cabang, b.ip_drc, b.ip_cabang, b.kode_bank, b.port_drc, b.port_cabang, b.paswword FROM p_ip_cabang a, p_port_cabang b WHERE a.kode_cabang = b.kode_cabang");
    }

    public function getAllRedisConnectionById($id_cabang)
    {
        return $this->query("SELECT a.kode_cabang , a.short_name_cabang, b.ip_drc, b.ip_cabang, b.kode_bank, b.port_drc, b.port_cabang, b.paswword FROM p_ip_cabang a, p_port_cabang b WHERE a.kode_cabang = ".$id_cabang." AND b.kode_cabang = ".$id_cabang." AND b.kode_bank = 'MAIN'")[0];
    }

    public function getPathDataLalin($id_cabang)
    {
        return $this->query("SELECT PKG_UPLOAD_DATA_LALIN.f_001getuploadpath (".$id_cabang.") FROM DUAL")[0]->path_lalin;
    }

    public function getLogReplikasi($id_cabang)
    {
        return $this->query("SELECT F_GET_STREAMS_HEARTBEAT($id_cabang) STREAMS_HEARTBEAT FROM DUAL");
    }

    public function submitReplikasiPKG($status, $kode_cabang)
    {
        return $this->query("CALL pkg_status_replikasi.sp_submit_replikasi_db (".$status.", ".$kode_cabang.")");
    }

    public function portDcjm($kode_cabang)
    {
        return $this->query('SELECT * FROM PORT_API_DCJM WHERE "kode_cabang" = ' . $kode_cabang);
    }
}
