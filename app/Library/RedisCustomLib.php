<?php

namespace App\Library;

use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use Predis\Client;
use Predis\Command\CommandInterface;
use function MongoDB\BSON\toJSON;


class RedisCustomLib
{
    protected $oracle, $cLib, $client;

    public function __construct()
    {
        $this->oracle = new OracleCustomLib();
        $this->cLib   = new CustomLib();
    }

    public function redisConnection($port, $host, $password)
    {
        $redis = new Client(array(
            'host'     => $host,
            'port'     => $port,
            'password' => $password,
            'database' => 0,
        ));

        return $redis;
    }

    public function getReplicationByKey($port, $key, $host, $password)
    {
        try
        {
            $redis            = $this->redisConnection($port, $host, $password);
            $data_replication = $redis->info("replication");
            $finalArray       = array();

            if ($key == 'role')
            {
                return $data_replication['Replication']['role'];
            }
            else
            {
                foreach($data_replication['Replication'] as $index => $val)
                {
                    $finalArray[$index] = $val;
                }

                return isset($finalArray[$key]) ? number_format($finalArray[$key], 0, ',', '.') : "-";
            }
        }
        catch (\Exception $exception)
        {
            return "connection error";
        }
    }

    public function getDataReplication($id_cabang)
    {
        $auth = $this->oracle->getAllRedisConnection();
        $mode = $this->oracle->getReplikasiByKey($id_cabang, "status_repl");
        $data = array();

        foreach ($auth as $key => $value)
        {
            if ($value->kode_cabang == $id_cabang) {
                if ($value->kode_bank == "MAIN") $name = 'main';
                if ($value->kode_bank == "BBNI") $name = 'bni';
                if ($value->kode_bank == "BBRI") $name = 'bri';
                if ($value->kode_bank == "MDRI") $name = 'mandiri';
                if ($value->kode_bank == "BBCA") $name = 'bca';

                if ($mode->status_repl == 1)
                {
                    $data[$name]['slave']    = $this->getReplicationByKey($value->port_drc, 'slave_repl_offset', $value->ip_drc, $value->paswword);
                    $data[$name]['master']   = $this->getReplicationByKey($value->port_cabang, 'master_repl_offset', $value->ip_cabang, $value->paswword);
                    $data[$name]['status']   = $data[$name]['slave'] == $data[$name]['master'] ? 1 : 0;
                    $data[$name]['progress'] = $this->getReplicationByKey($value->port_drc, 'master_sync_in_progress', $value->ip_drc, $value->paswword);
                }
                else if ($mode->status_repl == 2)
                {
                    $data[$name]['slave']    = $this->getReplicationByKey($value->port_cabang, 'slave_repl_offset', $value->ip_cabang, $value->paswword);
                    $data[$name]['master']   = $this->getReplicationByKey($value->port_drc, 'master_repl_offset', $value->ip_drc, $value->paswword);
                    $data[$name]['status']   = $data[$name]['slave'] == $data[$name]['master'] ? 1 : 0;
                    $data[$name]['progress'] = $this->getReplicationByKey($value->port_drc, 'master_sync_in_progress', $value->ip_drc, $value->paswword);
                }
                else
                {
                    $data[$name]['slave']    = 0;
                    $data[$name]['master']   = 0;
                    $data[$name]['status']   = $data[$name]['slave'] == $data[$name]['master'] ? 1 : 0;
                    $data[$name]['progress'] = $this->getReplicationByKey($value->port_drc, 'master_sync_in_progress', $value->ip_drc, $value->paswword);
                }
            }
        }

        return $data;
    }

    public function getCurrentStatReplikasi($id_cabang)
    {
        $auth             = $this->oracle->getAllRedisConnectionById($id_cabang);
        $mode             = $this->oracle->getReplikasiByKey($id_cabang, "status_repl");
        $replikasi_cabang = $this->getReplicationByKey($auth->port_cabang, 'role', $auth->ip_cabang, $auth->paswword);
        $replikasi_drc    = $this->getReplicationByKey($auth->port_drc, 'role', $auth->ip_drc, $auth->paswword);
        $data             = array ('success' => true, 'status' => 0, 'message' => "ONPROGRESS");
        $off              = false;

        if ($replikasi_cabang == "master" &&$replikasi_drc == "master") $off = true;

        if ($mode->status_repl == 0 && $off == true) $data = array ('success' => true, 'status' => 1, 'message' => "OFF");
        if ($mode->status_repl == 1 && $replikasi_cabang == 'master') $data = array ('success' => true, 'status' => 1, 'message' => "MASTER");
        if ($mode->status_repl == 2 && $replikasi_cabang == 'slave') $data = array ('success' => true, 'status' => 1, 'message' => "SLAVE");
        if ($replikasi_cabang == 'connection error') $data = array ('success' => false, 'status' => 0, 'message' => $replikasi_cabang);

//        $data = array ('success' => true, 'status' => 1, 'message' => "OFF");

        return $data;
    }

    public function at4($port, $host, $password, $portApi)
    {
        $redis        = $this->redisConnection($port, $host, $password);
        $list_periode = $redis->keys('list_periode*');
//        $crnt_date    = $redis->get('current_tanggal');
//        $crnt_shift   = $redis->get('current_shift');
//        $crnt_stat    = $crnt_date . '0' . $crnt_shift;

        foreach ($list_periode as $key => $value)
        {
            $ex_stat     = (explode("_", $value));
            $list_keyat4 = (explode(";", $redis->hget('list_waiting', 'FPERIODE_' . $ex_stat[2] . '_' . $ex_stat[3])));

            foreach ($list_keyat4 as $key1 => $value1)
            {
                $result_keyat4    = $redis->get($value1);
                $ex_result_keyat4 = explode(";", $result_keyat4);

                if (count($ex_result_keyat4) < 2)
                {
                    $plainkey            = substr($result_keyat4, 0, 27);
                    $jumlah['RpMandiri'] = $redis->get($plainkey . '003');
                    $jumlah['RpBri']     = $redis->get($plainkey . '020');
                    $jumlah['RpBni']     = $redis->get($plainkey . '021');
                    $jumlah['RpBca']     = $redis->get($plainkey . '023');
                    $parsedkey           = $this->cLib->parsingDataKeyAT4($result_keyat4, $jumlah);

                    $this->cLib->sendJson('http://10.122.1.5:'.$portApi.'/api/data/atb4', $parsedkey);
                }
                else
                {
                    foreach ($ex_result_keyat4 as $key2 => $value2)
                    {
                        $plainkey            = substr($value2, 0, 27);
                        $jumlah['RpMandiri'] = $redis->get($plainkey . '003');
                        $jumlah['RpBri']     = $redis->get($plainkey . '020');
                        $jumlah['RpBni']     = $redis->get($plainkey . '021');
                        $jumlah['RpBca']     = $redis->get($plainkey . '023');
                        $parsedkey           = $this->cLib->parsingDataKeyAT4($value2, $jumlah);

                        $this->cLib->sendJson(env('URL_API_DRC', 'http://10.122.1.5').':'.$portApi.'/api/data/atb4', $parsedkey); // {"status":{"code":0,"message":"Berhasil diterima, data dalam proses komparasi."}}
                    }
                }
            }
        }

        return TRUE;
    }
}
