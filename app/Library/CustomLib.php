<?php

namespace App\Library;

use Illuminate\Support\Facades\DB;

class CustomLib
{
    public function dateNow()
    {
        return date('Y-m-d H:i:s');
    }

    public function setUsedCabang($cabang)
    {
        return DB::table('used_cabang')->update(['id_cabang' => $cabang]);
    }

    public function getUsedCabang()
    {
        return DB::table('used_cabang')->get()[0]->id_cabang;
    }

    public function parsingDataKeyAT4($key, $jumlah)
    {
        $exMandiri = $jumlah['RpMandiri'] == NULL ? "0:0" : explode(":", $jumlah['RpMandiri']);
        $exBri = $jumlah['RpBri'] == NULL ? "0:0" : explode(":", $jumlah['RpBri']);
        $exBni = $jumlah['RpBni'] == NULL ? "0:0" : explode(":", $jumlah['RpBni']);
        $exBca = $jumlah['RpBca'] == NULL ? "0:0" : explode(":", $jumlah['RpBca']);

        $kodeintegrator = substr($key, 0, 3) . "|D";
        $cabang         = substr($key, 3, 3);
        $gerbang        = substr($key, 6, 3);
        $gardu          = substr($key, 9, 3);
        $year           = substr($key, 12, 4);
        $month          = substr($key, 16, 2);
        $day            = substr($key, 18, 2);
        $tanggal        = $year . '-' . $month . '-' . $day;
        $shift          = substr($key, 20, 2);
        $golongan       = substr($key, 22, 2);
        $asal_gerbang   = substr($key, 24, 3);
        $idbank         = substr($key, 27, 3);
        $stat           = substr($key, 30, 4);

        $data = array(
            "IdCabang"       => (int) $cabang,
            "IdGerbang"      => (int) $gerbang,
            "Tanggal"        => $tanggal,
            "Shift"          => (int) $shift,
            "IdGardu"        => (int) $gardu,
            "Golongan"       => (int) $golongan,
            "IdAsalGerbang"  => (int) $asal_gerbang,
            "Tunai"          => 0,
            "DinasOpr"       => 0,
            "DinasMitra"     => 0,
            "DinasKary"      => 0,
            "eMandiri"       => (int) $exMandiri[0],
            "eBri"           => (int) $exBri[0],
            "eBni"           => (int) $exBni[0],
            "eBca"           => (int) $exBca[0],
            "eMega"          => 0,
            "eNobu"          => 0,
            "eDKI"           => 0,
            "RpTunai"        => 0,
            "RpeMandiri"     => (int) $exMandiri[1],
            "RpeBri"         => (int) $exBri[1],
            "RpeBni"         => (int) $exBni[1],
            "RpeBca"         => (int) $exBca[1],
            "RpeMega"        => 0,
            "RpeNobu"        => 0,
            "RpeDKI"         => 0,
            "RpDinasMitra"   => 0,
            "RpDinasKary"    => 0,
            "Lolos"          => 0,
            "Indamal"        => 0,
            "Mjmdr"          => 0,
            "Ags"            => 0,
            "KodeIntegrator" => $kodeintegrator
        );

        return json_encode($data, true);
    }

    public function sendJson($url, $data)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $result = curl_exec($ch);

        return $result;
    }
}
