<?php

namespace App\Http\Controllers;

use App\Library\CustomLib;
use App\Library\OracleCustomLib;
use App\Library\RedisCustomLib;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DataLalinController extends Controller
{
    protected $cLib, $oracle, $redis;

    public function __construct()
    {
        $this->cLib   = new CustomLib();
        $this->oracle = new OracleCustomLib();
        $this->redis  = new RedisCustomLib();
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function upload()
    {
        $data['current_cabang'] = $this->cLib->getUsedCabang();
        $data['cabang'] = $this->oracle->getCabang();

        return view('datalalin_upload', $data);
    }

    public function uploadProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file_txt'     => 'required',
            'file_control' => 'required'
        ]);

        $file_txt     = $request->file('file_txt');
        $file_control = $request->file('file_control');

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
        if ($file_txt->getClientOriginalExtension() != 'txt') return redirect()->back()->withErrors("Extension file input txt not valid")->withInput();
        if ($file_control->getClientOriginalExtension() != 'control') return redirect()->back()->withErrors("Extension file input control not valid")->withInput();
        if (sizeof(explode(";", $file_txt->openFile())) != 50) return redirect()->back()->withErrors("Data not valid")->withInput();

        $destination = $this->oracle->getPathDataLalin($request['id_cabang']);

        if ($file_txt->move($destination, $file_txt->getClientOriginalName()))
        {
            if ($file_control->move($destination, $file_control->getClientOriginalName()))
            {
                chmod($destination .'/'. $file_txt->getClientOriginalName(), 0775);
                chmod($destination .'/'. $file_control->getClientOriginalName(), 0775);

                return redirect('/datalalin/upload')->with('success', 'File uploaded successfully');
            }
        }
    }
}
