<?php

namespace App\Http\Controllers;

use App\Library\CustomLib;
use App\Library\OracleCustomLib;
use App\Library\RedisCustomLib;
use Illuminate\Http\Request;

class ReplikasiController extends Controller
{
    protected $cLib, $oracle, $redis;

    public function __construct(Request $request)
    {
        $this->cLib   = new CustomLib();
        $this->oracle = new OracleCustomLib();
        $this->redis  = new RedisCustomLib();

        $this->storeRequest($request);
    }

    public function ajaxData(Request $request)
    {
        $this->cLib->setUsedCabang($request['id_cabang']);
        $data['data']       = $this->redis->getDataReplication($request['id_cabang']);
        $data['mode']       = $this->oracle->getReplikasi($request['id_cabang']);
        $data['log']        = $this->oracle->getLogReplikasi($request['id_cabang']);
        $data['log_status'] = is_array($this->oracle->getLogReplikasi($request['id_cabang']));

        return $data;
    }

    public function direct_ajaxData(Request $request)
    {
        $this->cLib->setUsedCabang($request['id_cabang']);
        $data['data']       = $this->redis->getDataReplication($request['id_cabang']);
        $data['mode']       = $this->oracle->getReplikasi($request['id_cabang']);
        $data['log']        = $this->oracle->getLogReplikasi($request['id_cabang']);
        $data['log_status'] = is_array($this->oracle->getLogReplikasi($request['id_cabang']));

        return $data;
    }

    public function index(Request $request)
    {
        $data['cabang']         = $this->oracle->getCabang();
        $data['current_cabang'] = $data['cabang'][0]->kode_cabang;
        $data['log']            = $this->oracle->getLogReplikasi($data['current_cabang']);
        $data['mode']           = $this->oracle->getReplikasi($data['current_cabang']);
        $data['content']        = $this->redis->getDataReplication($data['current_cabang']);
        $data['log_status']     = is_array($this->oracle->getLogReplikasi($data['current_cabang']));

        return view('replikasi', $data);
    }

    public function direct_replikasi(Request $request)
    {
        $data['cabang']         = $this->oracle->getCabang();
        $data['current_cabang'] = $data['cabang'][0]->kode_cabang;
        $data['log']            = $this->oracle->getLogReplikasi($data['current_cabang']);
        $data['mode']           = $this->oracle->getReplikasi($data['current_cabang']);
        $data['content']        = $this->redis->getDataReplication($data['current_cabang']);
        $data['log_status']     = is_array($this->oracle->getLogReplikasi($data['current_cabang']));

        return view('direct_replikasi', $data);
    }

    public function storeRequest(Request $request) 
    {
        $request->request->add(['display_cabang' => 3]);
    }

    public function test_replikasi()
    {
        return json_encode($this->redis->getDataReplication($this->cLib->getUsedCabang()));
    }
}
