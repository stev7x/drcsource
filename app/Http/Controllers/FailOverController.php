<?php

namespace App\Http\Controllers;

use App\Library\CustomLib;
use App\Library\OracleCustomLib;
use App\Library\RedisCustomLib;
use Illuminate\Http\Request;

class FailOverController extends Controller
{
    protected $cLib, $oracle, $redis, $key;

    public function __construct()
    {
        $this->cLib   = new CustomLib();
        $this->oracle = new OracleCustomLib();
        $this->redis  = new RedisCustomLib();
        $this->key    = "AUTH8n2tx387r27tnxn374t2xnm4r7t82ntr782r71nm4tr7";
    }

    public function ajaxData(Request $request)
    {
        $this->cLib->setUsedCabang($request['id_cabang']);

        $data['data']            = @$this->oracle->getLogDRC($request['id_cabang']);
        $data['mode']            = $this->oracle->getReplikasi($request['id_cabang']);
        $data['cstat_replikasi'] = @$this->redis->getCurrentStatReplikasi($request['id_cabang']);
        $data['cstat_drc']       = $data['mode']->status_drc == $data['mode']->real_stat_drc ? ($data['mode']->status_drc == 0 ? "STANDBY" : "ONLINE") : "ONPROGRESS";
        $data['status']          = @$data['data'][0]->delay_time >= 1800000 ? 0 : 1;
        $data['note']            = $request['id_cabang'] == 33 ? "*JBT menggunakan waktu WITA" : "";

        return $data;
    }

    public function ajaxChangeMode(Request $request)
    {
        $result = $this->oracle->updateReplikasi($request['id_cabang'], $request['key'], $request['status']);
        $this->oracle->submitReplikasiPKG($request['status'], $request['id_cabang']);

        return $result;
    }

    public function index(Request $request)
    {
//        print_r($request['display_cabang']);
//        exit;

        $data['current_cabang']  = $this->cLib->getUsedCabang();
        $data['cabang']          = $this->oracle->getCabang();
        $data['mode']            = $this->oracle->getReplikasi($data['current_cabang']);
        $data['data']            = $this->oracle->getLogDRC($data['current_cabang']);
        $data['cstat_replikasi'] = @$this->redis->getCurrentStatReplikasi($data['current_cabang']);
        $data['cstat_drc']       = ($data['mode']->status_drc == $data['mode']->real_stat_drc) ? ($data['mode']->status_drc == 0 ? "STANDBY" : "ONLINE") : "ONPROGRESS";
        $data['status']          = @$data['data'][0]->delay_time >= 1800000 ? 0 : 1;
        $data['note']            = $data['current_cabang'] == 33 ? "*JBT menggunakan waktu WITA" : "";

        return view('failover', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function testAT4(Request $request)
    {
        $data    = $this->oracle->getRedisConnection($request['id_cabang']);
        $portApi = $this->oracle->portDcjm($request['id_cabang'])[0]->port;

        if ($portApi == null) return "failed";

        if ($this->redis->at4($data->port_drc, $data->ip_drc, $data->paswword, $portApi))
        {
            return "success";
        }
        else
        {
            return "failed";
        }
    }

    public function currentStatReplikasi(Request $request)
    {
        return $this->redis->getCurrentStatReplikasi($request['id_cabang']);
    }

    public function currentStatDRC(Request $request)
    {
        $replikasi = $this->oracle->getReplikasi($request['id_cabang']);

        return $data = array(
            'success' => true,
            'status' => $replikasi->status_drc == $replikasi->real_stat_drc ? 1 : 0,
            'message' => $replikasi->status_drc == $replikasi->real_stat_drc ? ($replikasi->status_drc == 0 ? "STANDBY" : "ONLINE") : "ONPROGRESS"
        );
    }

    public function heartbeat(Request $request)
    {
        if (!isset($request['id_cabang']))
            return json_encode(
                $data = array(
                    'success' => false,
                    'message' => "ID CABANG is missing",
                    'data' => null),
                true);

        if (is_null($request['id_cabang']))
            return json_encode(
                $data = array(
                    'success' => false,
                    'message' => "ID CABANG cannot be null",
                    'data' => null),
                true);

        if (!isset($request['key']))
            return json_encode(
                $data = array(
                    'success' => false,
                    'message' => "KEY is missing",
                    'data' => null),
                true);

        if ($this->key != $request['key'])
            return json_encode(
                $data = array(
                    'success' => false,
                    'message' => "authentication failed",
                    'data' => null),
                true);

        $heartbeat = $this->oracle->getLogDRC($request['id_cabang']);

        $data = array(
            'success' => true,
            'message' => $this->oracle->getCabangName($request['id_cabang']),
            'data' => array ($heartbeat[0]->tgl_terima, $heartbeat[1]->tgl_terima));

        return json_encode($data, true);
    }
}
