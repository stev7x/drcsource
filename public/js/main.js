function onLoadCheckLocalStorage() {
    let selectedCabang = $("#cabang option:selected").val();
    let currentCartCounter = localStorage.getItem('display_cabang');

    if (currentCartCounter === null) localStorage.setItem('display_cabang', selectedCabang);
}

onLoadCheckLocalStorage();