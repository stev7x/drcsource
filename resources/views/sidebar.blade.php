<!-- ============================================================== -->
<!-- left sidebar -->
<!-- ============================================================== -->
<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li class="nav-item">
                        {{--<a class="nav-link {{ (request()->is('home*')) ? 'active' : '' }}" href="{{ route('home') }}">--}}
                            {{--<i class="fas fa-tachometer-alt"></i>Home--}}
                        {{--</a>--}}
                        <a class="nav-link {{ (request()->is('replikasi*')) ? 'active' : '' }}" href="{{ route('replikasi') }}">
                            <i class="fas fa-database"></i>Status Replikasi
                        </a>
                        <a class="nav-link {{ (request()->is('failover*')) ? 'active' : '' }}" href="{{ route('failover') }}">
                            <i class="fas fa-circle-notch"></i>Fail Over
                        </a>
                        <a class="nav-link {{ (request()->is('datalalin*')) ? 'active' : '' }}" href="{{ route('datalalin_upload') }}">
                            <i class="fas fa-upload"></i>Upload Data Lalin
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- ============================================================== -->
<!-- end left sidebar -->
<!-- ============================================================== -->