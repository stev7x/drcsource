@extends('main')

@section('content')
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->

    <input type="hidden" id="current_db_replikasi" value="{{ $mode->status_repl }}"/>
    <input type="hidden" id="current_redis_replikasi" value="{{ $cstat_replikasi['message'] }}"/>
    <input type="hidden" id="current_redis_drc" value="{{ $cstat_drc }}"/>

    <div class="dashboard-wrapper">
        <div class="container-fluid  dashboard-content">
            <!-- ============================================================== -->
            <!-- pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Fail Over</h2>
                        <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    @php($segments = '')
                                    @foreach(Request::segments() as $segment)
                                        @php($segments .= '/'.$segment)
                                        <li class="breadcrumb-item">
                                            <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                                        </li>
                                    @endforeach
                                    {{--<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>--}}
                                    {{--<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Tables</a></li>--}}
                                    {{--<li class="breadcrumb-item active" aria-current="page">Data Tables</li>--}}
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="progress" style="height: 2px;">
                        <div class="progress-bar" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                    </div>

                    <div class="card">
                        <h5 class="card-header">
                            <div style="float: left">
                                <u id="used_cabang_name">cabang</u>
                            </div>

                            <select class="custom-select" id="cabang" style="max-width: 150px; float: right">
                                @foreach(@$cabang as $key => $value)
                                    <option value="{{ $value->kode_cabang }}" {{ $value->kode_cabang == $current_cabang ? "selected" : "" }}>
                                        {{ $value->short_name_cabang }}
                                    </option>
                                @endforeach
                            </select>
                        </h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <span style="font-size: 14px">Status DRC <span id="redis_stat_drc" style="color: red">({{ $cstat_drc }})</span></span>
                                    <fieldset>
                                        <div class="switch-toggle switch-holo">
                                            <input id="drc_offline" name="drc_status" value="0" type="radio" {{ $mode->status_drc == 0 ? 'checked' : '' }}>
                                            <label class="loff_label" for="drc_offline" onclick="confirmDRC()" style="color: {{ $mode->status_drc == 0 ? '#fff' : '' }}"><b>STANDBY</b></label>

                                            <input id="drc_online" name="drc_status" value="1" type="radio" {{ $mode->status_drc == 1 ? 'checked' : '' }}>
                                            <label class="lon_label" for="drc_online" onclick="confirmDRC()" style="color: {{ $mode->status_drc == 1 ? '#fff' : '' }}"><b>ONLINE</b></label>

                                            <a id="div_status" style="background-color: {{ $mode->status_drc == 0 ? '#ffca28' : '#4caf50' }};"></a>
                                        </div>
                                    </fieldset>
                                </div>

                                {{--<div class="col-md-3">--}}
                                    {{--<span style="font-size: 14px">Status Cabang</span>--}}
                                    {{--<fieldset>--}}
                                        {{--<div class="switch-toggle switch-holo grey">--}}
                                            {{--<input id="cab_offline" name="cab_status" value="0" type="radio" {{ $mode->status_cab == 0 ? 'checked' : '' }}>--}}
                                            {{--<label for="cab_offline" onclick="confirmCAB()" style="color: {{ $mode->status_cab == 0 ? '#fff' : '' }}"><b>OFFLINE</b></label>--}}

                                            {{--<input id="cab_online" name="cab_status" value="1" type="radio" {{ $mode->status_cab == 1 ? 'checked' : '' }}>--}}
                                            {{--<label for="cab_online" onclick="confirmCAB()" style="color: {{ $mode->status_cab == 1 ? '#fff' : '' }}"><b>ONLINE</b></label>--}}

                                            {{--<a id="div_status_cabang" style="background-color: {{ $mode->status_cab == 0 ? '#f44336' : '#4caf50' }};"></a>--}}
                                        {{--</div>--}}
                                    {{--</fieldset>--}}
                                {{--</div>--}}

                                <div class="col-md-6">
                                    <span style="font-size: 14px">Status Replikasi <span id="redis_stat_replikasi" style="color: red">({{ $cstat_replikasi['message'] }})</span> </span>
                                    <fieldset>
                                        <div class="switch-toggle switch-holo">
                                            <input id="off" name="mode" value="0" type="radio" {{ $mode->status_repl == 0 ? 'checked' : '' }}>
                                            <label class="loff" for="off" onclick="confirmREPL(0)" style="color: {{ $mode->status_repl == 0 ? '#fff' : '' }}"><b>Off</b></label>

                                            <input id="master" name="mode" value="1" type="radio" {{ $mode->status_repl == 1 ? 'checked' : '' }}>
                                            <label class="lmaster" for="master" onclick="confirmREPL(1)" style="color: {{ $mode->status_repl == 1 ? '#fff' : '' }}"><b>Master</b></label>

                                            <input id="slave" name="mode" value="2" type="radio" {{ $mode->status_repl == 2 ? 'checked' : '' }}>
                                            <label class="lslave" for="slave" onclick="confirmREPL(2)" style="color: {{ $mode->status_repl == 2 ? '#fff' : '' }}"><b>Slave</b></label>

                                            <a id="div_status_replikasi" style="background-color: {{ $mode->status_repl == 0 ? '#f44336' : '#4caf50' }};"></a>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-2">
                                    <span style="font-size: 14px">Manual End Of Shift</span>
                                    <a href="#" onclick="confirmEOS()" class="btn btn-outline-light btn-block btn-sm">Run EOS</a>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalREPL" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedREPL()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Anda yakin ingin merubah Status Replikasi ? </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedREPL()">No</a>
                                        <a href="#" class="btn btn-primary" onclick="ConfirmedREPL()">Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalDRC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedDRC()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Anda yakin ingin merubah Status DRC ? </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedDRC()">No</a>
                                        <a href="#" class="btn btn-primary" onclick="ConfirmedDRC()">Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalCAB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedCAB()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Anda yakin ingin merubah Status Cabang ? </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedCAB()">No</a>
                                        <a href="#" class="btn btn-primary" onclick="ConfirmedCAB()">Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalEOS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedEOS()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Anda yakin ingin melakukan proses End of Shift ? </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedEOS()">No</a>
                                        <a href="#" class="btn btn-primary" onclick="ConfirmedEOS()">Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalWarningReplikasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">WARNING</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedREPL()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Status Replikasi harus <b style="color: red">OFF</b> sebelum diganti menjadi <b style="color: green">MASTER / SLAVE</b></p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedREPL()">Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalWarningReplikasiOnProgress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">WARNING</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedREPL()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Status Replikasi tidak bisa diubah selama status ONPROGRESS</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedREPL()">Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="modalWarningDRCOnProgress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">WARNING</h5>
                                        <a href="#" class="close" data-dismiss="modal" aria-label="Close" onclick="unConfirmedDRC()">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                    <div class="modal-body">
                                        <p>Status DRC tidak bisa diubah selama status ONPROGRESS</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#" class="btn btn-secondary" onclick="unConfirmedDRC()">Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <span id="note"><i>{{ $note }}</i></span>
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        <tr align="center">
                                            <th>Tanggal Kirim Cabang</th>
                                            <th>Tanggal Terima</th>
                                            <th>Tanggal Terima DRC</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data">
                                    @if($data == null)
                                        <tr align="center">
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                        </tr>
                                    @else
                                        @foreach($data as $key => $value)
                                            <tr align="center">
                                                <td>{{ $value->cabang_datetime }}</td>
                                                <td>{{ $value->tgl_terima }}</td>
                                                <td>{{ $value->delay_time }} s</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end fixed header  -->
                <!-- ============================================================== -->
            </div>
        </div>

        <script type="text/javascript">
            $( document ).ready(function() {
                // let a = $('#current_db_replikasi').val(), b = $('#current_redis_replikasi').val();
                // if (a === 0 && b === "OFF") console.log("a");

                if ($("#cabang option:selected").text() !== "DRC") {
                    $('#used_cabang_name').text($("#cabang option:selected").text());
                    $('#cabang_title').text($("#cabang option:selected").text());
                    $('#time_cabang_name').text($("#cabang option:selected").text());
                }

                $('#cabang').change(function () {
                    let id_cabang = $("#cabang option:selected").val();
                    let nama_cabang = $("#cabang option:selected").text();
                    let row = "";
                    let progress_bar = $(".progress-bar");

                    progress_bar.attr("style", "width: 100%");

                    $.ajax({
                        url: '{{ url("/failover/ajaxdata?id_cabang=") }}' + id_cabang,
                        success: function(data){
                            console.log(data);
                            $('#used_cabang_name').text(nama_cabang);
                            $('#cabang_title').text(nama_cabang);
                            $('#time_cabang_name').text(nama_cabang);
                            $('#redis_stat_drc').text("("+data.cstat_drc+")");
                            $('#redis_stat_replikasi').text("("+data.cstat_replikasi.message+")");
                            $("input[name='mode'][value='" + data.mode + "']").prop("checked", true);
                            $("#note").text(data.note);
                            $('#current_db_replikasi').val(data.mode.status_repl);
                            $('#current_redis_replikasi').val(data.cstat_replikasi.message);
                            $('#current_redis_drc').val(data.cstat_drc);

                            // console.log(data.mode.status_drc + " - " + data.mode.status_cab + " - " + data.mode.status_repl);
                            // console.log($('#current_db_replikasi').val() + ", " + $('#current_redis_replikasi').val());

                            $('input:radio[name="drc_status"]').filter('[value="1"]').attr('checked', false);
                            $('input:radio[name="drc_status"]').filter('[value="2"]').attr('checked', false);

                            $('input:radio[name="mode"]').filter('[value="0"]').attr('checked', false);
                            $('input:radio[name="mode"]').filter('[value="1"]').attr('checked', false);
                            $('input:radio[name="mode"]').filter('[value="2"]').attr('checked', false);

                            $('.lon_label').removeAttr("style");
                            $('.loff_label').removeAttr("style");
                            $('#div_status').removeAttr("style");
                            $('#on_label').removeAttr("style");
                            $('#off_label').removeAttr("style");

                            $('#div_status_replikasi').removeAttr("style");
                            $('.loff').removeAttr("style");
                            $('.lmaster').removeAttr("style");
                            $('.lslave').removeAttr("style");
                            $('#off').removeAttr("style");
                            $('#master').removeAttr("style");
                            $('#slave').removeAttr("style");

                            if (data.mode.status_drc == 1) {
                                $('.lon_label').css("color", "#fff");
                                $('input:radio[name="drc_status"]').filter('[value="1"]').attr('checked', true);
                                $('#div_status').css("background-color", "#4caf50");
                            } else {
                                $('.loff_label').css("color", "#fff");
                                $('input:radio[name="drc_status"]').filter('[value="0"]').attr('checked', true);
                                $('#div_status').css("background-color", "#ffca28");
                            }

                            if (data.mode.status_repl == 0) {
                                $('.loff').css("color", "#fff");
                                $('input:radio[name="mode"]').filter('[value="0"]').attr('checked', true);
                                $('#div_status_replikasi').css("background-color", "#f44336");
                            } else if (data.mode.status_repl == 1) {
                                $('.lmaster').css("color", "#fff");
                                $('input:radio[name="mode"]').filter('[value="1"]').attr('checked', true);
                                $('#div_status_replikasi').css("background-color", "#4caf50");
                            } else {
                                $('.lslave').css("color", "#fff");
                                $('input:radio[name="mode"]').filter('[value="2"]').attr('checked', true);
                                $('#div_status_replikasi').css("background-color", "#4caf50");
                            }

                            $('#data').html('');
                            if (data.data == null) {
                                row += "<tr align='center'>";
                                row += "<td>-</td>";
                                row += "<td>-</td>";
                                row += "<td>-</td>";
                                row += "</tr>";
                            } else {
                                $.each(data.data, function(i, item) {
                                    row += "<tr align='center'>";
                                    row += "<td>" + item.cabang_datetime + "</td>";
                                    row += "<td>" + item.tgl_terima + "</td>";
                                    row += "<td>" + item.delay_time + " ms</td>";
                                    row += "</tr>";
                                });
                            }
                            $('#data').append(row);

                            progress_bar.attr("style", "width: 0%");
                        }
                    });
                });
            });

            function confirmREPL(clicked) {
                let cDBReplikasi = $('#current_db_replikasi').val(),
                    cRedisReplikasi = $('#current_redis_replikasi').val();

                if (cRedisReplikasi === "ONPROGRESS") {
                    $('#modalWarningReplikasiOnProgress').modal('show');
                } else if (cDBReplikasi === 0 && cRedisReplikasi === "OFF") {
                    $('#modalREPL').modal('show');
                } else if (clicked === 0) {
                    $('#modalREPL').modal('show');
                } else {
                    $('#modalWarningReplikasi').modal('show');
                }
            }

            function ConfirmedREPL() {
                let status = $("input[name='mode']:checked").val();
                let id_cabang = $("#cabang option:selected").val();

                $.ajax({
                    url: '{{ url("/failover/ajaxchangemode?id_cabang=") }}' + id_cabang + '&key=status_repl&status=' + status,
                    success: function(data){
                        $('#modalREPL').modal('hide');
                        console.log(data);
                        console.log("Success change mode to " + status);

                        $('.loff').removeAttr("style");
                        $('.lmaster').removeAttr("style");
                        $('.lslave').removeAttr("style");
                        $('#div_status_replikasi').removeAttr("style");
                        $('#off').removeAttr("style");
                        $('#master').removeAttr("style");
                        $('#slave').removeAttr("style");

                        if (status == 0) {
                            $('.loff').css("color", "#fff");
                            $('#div_status_replikasi').css("background-color", "#f44336");
                        } else if (status == 1) {
                            $('.lmaster').css("color", "#fff");
                            $('#div_status_replikasi').css("background-color", "#4caf50");
                        } else {
                            $('.lslave').css("color", "#fff");
                            $('#div_status_replikasi').css("background-color", "#4caf50");
                        }

                        $('#redis_stat_replikasi').text("(ONPROGRESS)");
                    }
                });
            }
            
            function unConfirmedREPL() {
                $("input[name='mode'][value='{{ $mode->status_repl }}']").prop("checked", true);

                $('#modalREPL').modal('hide');
                $('#modalWarningReplikasi').modal('hide');
                $('#modalWarningReplikasiOnProgress').modal('hide');
            }

            function checkCurrentStatDRC(id_cabang, counter) {
                let status = 0;

                setTimeout(function () {
                    $.ajax({
                        url: '{{ url("/failover/currentstatdrc?id_cabang=") }}' + id_cabang,
                        success: function(data){
                            $('#modalREPL').modal('hide');
                            console.log(data);
                            console.log("Current Redis status " + data);

                            status = data.status;
                            $('#redis_stat_drc').text(data.message);
                        }
                    });
                }, counter * 3000);

                return status;
            }

            function confirmDRC() {
                let cRedisDRC = $('#current_redis_drc').val();

                if (cRedisDRC === "ONPROGRESS") {
                    $('#modalWarningDRCOnProgress').modal('show');
                } else {
                    $('#modalDRC').modal('show');
                }
            }

            function ConfirmedDRC() {
                let status = $("input[name='drc_status']:checked").val();
                let id_cabang = $("#cabang option:selected").val();

                $.ajax({
                    url: '{{ url("/failover/ajaxchangemode?id_cabang=") }}' + id_cabang + '&key=status_drc&status=' + status,
                    success: function(data){
                        $('#modalDRC').modal('hide');
                        console.log(data);
                        console.log("Success change status DRC to " + status);

                        $('.lon_label').removeAttr("style");
                        $('.loff_label').removeAttr("style");
                        $('#div_status').removeAttr("style");
                        $('#on_label').removeAttr("style");
                        $('#off_label').removeAttr("style");

                        if (status == 1) {
                            $('.lon_label').css("color", "#fff");
                            $('#div_status').css("background-color", "#4caf50");
                        } else {
                            $('.loff_label').css("color", "#fff");
                            $('#div_status').css("background-color", "#ffca28");
                        }

                        $('#redis_stat_drc').text("(ONPROGRESS)");
                    }
                });
            }

            function unConfirmedDRC() {
                $("input[name='drc_status'][value='{{ $mode->status_drc }}']").prop("checked", true);
                $('#modalDRC').modal('hide');
                $('#modalWarningDRCOnProgress').modal('hide');
            }

            function confirmEOS() {
                $('#modalEOS').modal('show');
            }

            function ConfirmedEOS() {
                let id_cabang = $("#cabang option:selected").val();
                let progress_bar = $(".progress-bar");

                progress_bar.attr("style", "width: 100%");

                $.ajax({
                    url: '{{ url("/failover/at4test?id_cabang=") }}' + id_cabang,
                    success: function(data){
                        console.log(data);
                        $('#modalEOS').modal('hide');
                        // if (data == 'success') {
                            alert("EOS on progress");
                        // } else {
                        //     alert("EOS proccess failed");
                        // }
                        progress_bar.attr("style", "width: 0%");
                    }
                });
            }

            function unConfirmedEOS() {
                $('#modalEOS').modal('hide');
            }
        </script>
@endsection