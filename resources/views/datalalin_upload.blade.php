@extends('main')

@section('content')
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
    <div class="dashboard-wrapper">
        <div class="container-fluid dashboard-content">
            <!-- ============================================================== -->
            <!-- pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Upload Data Lalin</h2>
                        <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    @php($segments = '')
                                    @foreach(Request::segments() as $segment)
                                        @php($segments .= '/'.$segment)
                                        <li class="breadcrumb-item">
                                            <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                                        </li>
                                    @endforeach
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Upload Form</h5>
                        <div class="card-body">

                            @if(session()->get('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session()->get('success') }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                </div>
                            @endif

                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ $error }}
                                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </div>
                                @endforeach
                            @endif

                            <form action="{{ url("datalalin/upload/process") }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="input-select" class="col-3 col-lg-2 col-form-label text-right">Cabang</label>
                                    <div class="col-9 col-lg-10">
                                        <select class="custom-select" id="input-select" name="id_cabang">
                                            @foreach($cabang as $key => $value)
                                                <option value="{{ $value->kode_cabang }}" {{ $value->kode_cabang == $current_cabang ? "selected" : "" }}>
                                                    {{ $value->short_name_cabang }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="file_txt" class="col-3 col-lg-2 col-form-label text-right">File TXT</label>
                                    <div class="col-9 col-lg-10">
                                        <input type="file" class="form-control" id="file_txt" name="file_txt">
                                        <p><i>*File lalin berisi data yang akan diupload dengan banyak field 50 dan delimiter ";"</i></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="file_control" class="col-3 col-lg-2 col-form-label text-right">File Control</label>
                                    <div class="col-9 col-lg-10">
                                        <input type="file" class="form-control" id="file_control" name="file_control">
                                        <p><i>*File kontrol yang berisi namafilelalin;jumlahrecordlalin</i></p>
                                    </div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection