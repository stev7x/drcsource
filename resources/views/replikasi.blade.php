@extends('main')

@section('content')
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
    <div class="dashboard-wrapper">
        <div class="container-fluid  dashboard-content">
            <!-- ============================================================== -->
            <!-- pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Status Replikasi</h2>
                        <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    @php($segments = '')
                                    @foreach(Request::segments() as $segment)
                                        @php($segments .= '/'.$segment)
                                        <li class="breadcrumb-item">
                                            <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                                        </li>
                                    @endforeach
                                    {{--<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>--}}
                                    {{--<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Tables</a></li>--}}
                                    {{--<li class="breadcrumb-item active" aria-current="page">Data Tables</li>--}}
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="progress" style="height: 2px;">
                        <div class="progress-bar" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                    </div>
                    <div class="card">
                        <h5 class="card-header">
                            <div style="float: left">
                                Status Redis <u id="used_cabang_name">cabang</u> : <span id="used_cabang_mode">status</span>
                            </div>
                            <select class="custom-select" id="cabang" style="max-width: 150px; float: right">
                                @foreach($cabang as $key => $value)
                                    <option value="{{ $value->kode_cabang }}" {{ $value->kode_cabang == $current_cabang ? "selected" : "" }}>
                                        {{ $value->short_name_cabang }}
                                    </option>
                                @endforeach
                            </select>
                        </h5>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-borde#2196f3 first">
                                    <thead>
                                    <tr align="center">
                                        <th>Bank</th>
                                        <th>DRC</th>
                                        <th id="cabang_title">NKJ</th>
                                        <th>Status</th>
                                        <th>Progress Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr align="center">
                                        <td>Main</td>
                                        <td id="main_slave">{{ $content['main']['slave'] }}</td>
                                        <td id="main_master">{{ $content['main']['master'] }}</td>
                                        <td id="main_status" style="color: {{ $content['main']['status']==1 ? '#4caf50' : '#2196f3' }};">{{ $content['main']['status']==1 ? '✔' : '❍' }}</td>
{{--                                        <td id="main_progress">{{ $content['main']['status']==1 ? 'On Progress' : 'Synced' }}</td>--}}
                                        <td id="main_progress">{{ $content['main']['status']==1 ? 'Synced' : 'On Progress' }}</td>
                                    </tr>
                                    <tr align="center">
                                        <td>BNI</td>
                                        <td id="bni_slave">{{ $content['bni']['slave'] }}</td>
                                        <td id="bni_master">{{ $content['bni']['master'] }}</td>
                                        <td id="bni_status" style="color: {{ $content['bni']['status']==1 ? '#4caf50' : '#2196f3' }};">{{ $content['bni']['status']==1 ? '✔' : '❍' }}</td>
{{--                                        <td id="bni_progress">{{ $content['bni']['status']==1 ? 'On Progress' : 'Synced' }}</td>--}}
                                        <td id="main_progress">{{ $content['bni']['status']==1 ? 'Synced' : 'On Progress' }}</td>
                                    </tr>
                                    <tr align="center">
                                        <td>BRI</td>
                                        <td id="bri_slave">{{ $content['bri']['slave'] }}</td>
                                        <td id="bri_master">{{ $content['bri']['master'] }}</td>
                                        <td id="bri_status" style="color: {{ $content['bri']['status']==1 ? '#4caf50' : '#2196f3' }};">{{ $content['bri']['status']==1 ? '✔' : '❍' }}</td>
{{--                                        <td id="bri_progress">{{ $content['bri']['status']==1 ? 'On Progress' : 'Synced' }}</td>--}}
                                        <td id="main_progress">{{ $content['bri']['status']==1 ? 'Synced' : 'On Progress' }}</td>
                                    </tr>
                                    <tr align="center">
                                        <td>Mandiri</td>
                                        <td id="mandiri_slave">{{ $content['mandiri']['slave'] }}</td>
                                        <td id="mandiri_master">{{ $content['mandiri']['master'] }}</td>
                                        <td id="mandiri_status" style="color: {{ $content['mandiri']['status']==1 ? '#4caf50' : '#2196f3' }};">{{ $content['mandiri']['status']==1 ? '✔' : '❍' }}</td>
{{--                                        <td id="mandiri_progress">{{ $content['mandiri']['status']==1 ? 'On progress' : 'Synced' }}</td>--}}
                                        <td id="main_progress">{{ $content['mandiri']['status']==1 ? 'Synced' : 'On Progress' }}</td>
                                    </tr>
                                    <tr align="center">
                                        <td>BCA</td>
                                        <td id="bca_slave">{{ $content['bca']['slave'] }}</td>
                                        <td id="bca_master">{{ $content['bca']['master'] }}</td>
                                        <td id="bca_status" style="color: {{ $content['bca']['status']==1 ? '#4caf50' : '#2196f3' }};">{{ $content['bca']['status']==1 ? '✔' : '❍' }}</td>
{{--                                        <td id="bca_progress">{{ $content['bca']['progress']==1 ? 'On Progress' : 'Synced' }}</td>--}}
                                        <td id="main_progress">{{ $content['bca']['status']==1 ? 'Synced' : 'On Progress' }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header">Status Replikasi</h5>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-borde#2196f3 first">
                                    <thead>
                                        <tr align="center">
                                            <th>Cabang Datetime</th>
                                            <th>DRC Datetime</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data">
                                        @if ($log_status)
                                            @foreach((array) $log as $key => $value)
                                                <tr align="center">
                                                    <td>{{ $value->cabang_datetime }}</td>
                                                    <td>{{ $value->drc_datetime }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr align="center">
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end fixed header  -->
                <!-- ============================================================== -->
            </div>
        </div>

        <script type="text/javascript">
            $( document ).ready(function() {
                if ($("#cabang option:selected").text() !== "DRC") {
                    $('#used_cabang_name').text($("#cabang option:selected").text());
                    $('#used_cabang_mode').text("{{ $mode->status_repl == 0 ? 'Off' : ($mode->status_repl == 1 ? 'Master' : 'Slave') }}");
                    $('#cabang_title').text($("#cabang option:selected").text());
                }

                $('#cabang').change(function () {
                    let id_cabang = $("#cabang option:selected").val();
                    let nama_cabang = $("#cabang option:selected").text();
                    let progress_bar = $(".progress-bar");
                    let row = "";

                    progress_bar.attr("style", "width: 100%");

                    $.ajax({
                        url: '{{ url("/replikasi/ajaxdata?id_cabang=") }}' + id_cabang,
                        success: function(data) {
                            console.log(data);

                            $('#used_cabang_name').text(nama_cabang);
                            $('#cabang_title').text(nama_cabang);
                            $('#used_cabang_mode').text(data.mode.status_repl == 0 ? 'Off' : (data.mode.status_repl == 1 ? 'Master' : 'Slave'));

                            $('#main_slave').text(data.data.main.slave);
                            $('#main_master').text(data.data.main.master);
                            $('#main_status').text(data.data.main.status == 1 ? '✔' : '❍');
                            if (data.data.main.status == 1) {
                                $('#main_status').css("color", "#4caf50");
                            } else {
                                $('#main_status').css("color", "#2196f3");
                            }
                            $('#main_progress').text(data.data.main.progress == 1 ? 'On Progress' : 'Synced');

                            $('#bni_slave').text(data.data.bni.slave);
                            $('#bni_master').text(data.data.bni.master);
                            $('#bni_status').text(data.data.bni.status == 1 ? '✔' : '❍');
                            if (data.data.bni.status == 1) {
                                $('#bni_status').css("color", "#4caf50");
                            } else {
                                $('#bni_status').css("color", "#2196f3");
                            }
                            $('#bni_progress').text(data.data.bni.progress == 1 ? 'On Progress' : 'Synced');

                            $('#bri_slave').text(data.data.bri.slave);
                            $('#bri_master').text(data.data.bri.master);
                            $('#bri_status').text(data.data.bri.status == 1 ? '✔' : '❍');
                            if (data.data.bri.status == 1) {
                                $('#bri_status').css("color", "#4caf50");
                            } else {
                                $('#bri_status').css("color", "#2196f3");
                            }
                            $('#bri_progress').text(data.data.bri.progress == 1 ? 'On Progress' : 'Synced');

                            $('#mandiri_slave').text(data.data.mandiri.slave);
                            $('#mandiri_master').text(data.data.mandiri.master);
                            $('#mandiri_status').text(data.data.mandiri.status == 1 ? '✔' : '❍');
                            if (data.data.mandiri.status == 1) {
                                $('#mandiri_status').css("color", "#4caf50");
                            } else {
                                $('#mandiri_status').css("color", "#2196f3");
                            }
                            $('#mandiri_progress').text(data.data.mandiri.progress == 1 ? 'On Progress' : 'Synced');

                            $('#bca_slave').text(data.data.bca.slave);
                            $('#bca_master').text(data.data.bca.master);
                            $('#bca_status').text(data.data.bca.status == 1 ? '✔' : '❍');
                            if (data.data.bca.status == 1) {
                                $('#bca_status').css("color", "#4caf50");
                            } else {
                                $('#bca_status').css("color", "#2196f3");
                            }
                            $('#bca_progress').text(data.data.bca.progress == 1 ? 'On Progress' : 'Synced');

                            console.log(data.log_status);

                            $('#data').html('');
                            if (data.log_status === false) {
                                row += "<tr align='center'>";
                                row += "<td>-</td>";
                                row += "<td>-</td>";
                                row += "</tr>";
                            } else {
                                $.each(data.log, function(i, item) {
                                    row += "<tr align='center'>";
                                    row += "<td>" + item.cabang_datetime + "</td>";
                                    row += "<td>" + item.drc_datetime + "</td>";
                                    row += "</tr>";
                                });
                            }
                            $('#data').append(row);

                            progress_bar.attr("style", "width: 0%");
                        },
                    });
                });
            });
        </script>
@endsection